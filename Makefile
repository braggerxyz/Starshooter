CC=gcc
EXE=starshooter
CFLAGS=-Wall -Wextra -std=c17 -pedantic
LDFLAGS=-lSDL2 -lSDL2_image

build:
	$(CC) src/*.c -o $(EXE) $(CFLAGS) $(LDFLAGS)

run: build
	./$(EXE)

clean:
	rm $(EXE)
