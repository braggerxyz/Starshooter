#ifndef defs_h
#define defs_h

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 1024
#define PlAYER_SPEED 4
#define PLAYER_BULLET_SPEED 16
#define MAX_KEYBOARD_KEYS 350

#endif