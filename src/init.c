#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "init.h"
#include "structs.h"
#include "defs.h"

extern App app;

void initSDL(void) {
    const int renderFlags = SDL_RENDERER_ACCELERATED;
    const int windowFlags = 0;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Could not initialize SDL: %s\n", SDL_GetError());
        exit(1);
    }

    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);

    app.window = SDL_CreateWindow("Space Shooter", SDL_WINDOWPOS_UNDEFINED,
                                  SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                                  SCREEN_HEIGHT, windowFlags);
    if (!app.window) {
        fprintf(stderr, "Error creating SDL Window: %s\n", SDL_GetError());
        exit(2);
    }
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
    app.renderer = SDL_CreateRenderer(app.window, -1, renderFlags);
    if (!app.renderer) {
        fprintf(stderr, "Error creating SDL Renderer: %s\n", SDL_GetError());
        exit(3);
    }
}

void cleanup(void) {
    IMG_Quit();
    SDL_DestroyRenderer(app.renderer);
    SDL_DestroyWindow(app.window);
    SDL_Quit();
}