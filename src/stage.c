#include "stage.h"
#include "defs.h"
#include "draw.h"
#include "stdio.h"
#include "structs.h"

extern App app;
extern Stage stage;

static void logic(void);
static void draw(void);
static void initPlayer(void);

static Entity* player;
static SDL_Texture* atlas;

void initStage(void) {
    app.delegate.logic = logic;
    app.delegate.draw = draw;

    memset(&stage, 0, sizeof(Stage));
    stage.fighterTail = &stage.fighterHead;
    stage.bulletTail = &stage.bulletHead;

    initPlayer();
    atlas = loadTexture("assets/tilesheet.png");
}

static void initPlayer(void) {
    player = malloc(sizeof(Entity));
    memset(player, 0, sizeof(Entity));
    stage.fighterTail->next = player;
    stage.fighterTail = player;

    player->x = 100;
    player->y = 100;
    player->health = 1;
    SDL_Rect playerTile = {.x = 211, .y = 941, .w = 99, .h = 75};
    player->tile = playerTile;
}

static void fireBullet(void) {
    Entity* bullet;
    bullet = malloc(sizeof(Entity));
    memset(bullet, 0, sizeof(Entity));
    stage.bulletTail->next = bullet;
    stage.bulletTail = bullet;

    bullet->x = player->x;
    bullet->y = player->y;
    bullet->dx = PLAYER_BULLET_SPEED;
    bullet->health = 1;
    SDL_Rect bulletTile = {.x = 856, .y = 421, .w = 9, .h = 54};
    bullet->tile = bulletTile;
    bullet->y += (player->h / 2.f) - (bullet->h / 2.f);
    player->reload = 12;
}

static void doPlayer(void) {
    player->dx = player->dy = 0;
    if (player->reload > 0) {
        player->reload--;
    }

    if (app.keyboard[SDL_SCANCODE_W]) {
        player->dy = -PlAYER_SPEED;
    }
    if (app.keyboard[SDL_SCANCODE_S]) {
        player->dy = PlAYER_SPEED;
    }
    if (app.keyboard[SDL_SCANCODE_A]) {
        player->dx = -PlAYER_SPEED;
    }
    if (app.keyboard[SDL_SCANCODE_D]) {
        player->dx = PlAYER_SPEED;
    }

    if (app.keyboard[SDL_SCANCODE_LCTRL] && player->reload == 0) {
        fireBullet();
    }

    player->x += player->dx;
    player->y += player->dy;
}

static void doBullets(void) {
    Entity* b;
    Entity* prev;

    prev = &stage.bulletHead;

    for (b = stage.bulletHead.next; b != NULL; b = b->next) {
        b->x += b->dx;
        b->y += b->dy;

        if (b->x > SCREEN_WIDTH) {
            if (b == stage.bulletTail) {
                stage.bulletTail = prev;
            }
            prev->next = b->next;
            free(b);
            b = prev;
        }

        prev = b;
    }
}
static void logic(void) {
    doPlayer();
    doBullets();
}

static void drawPlayer(void) {
    SDL_Rect dest = {.x = player->x, .y = player->y, .w = player->tile.w, .h = player->tile.h};
    blit(atlas, player->tile, dest);
}

static void drawBullets(void) {
    Entity* b;

    for (b = stage.bulletHead.next; b != NULL; b = b->next) {
        SDL_Rect dest = {.x = b->x, .y = b->y, .w = b->tile.w, .h = b->tile.h};
        blit(atlas, b->tile, dest);
    }
}

static void draw(void) {
    drawPlayer();
    drawBullets();
}