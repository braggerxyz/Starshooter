#ifndef DRAW_H
#define DRAW_H

#include <SDL2/SDL.h>

void prepareScene(void);
void presentScene(void);
SDL_Texture* loadTexture(char* filename);
void blit(SDL_Texture* texture, SDL_Rect src, SDL_Rect dst);

#endif