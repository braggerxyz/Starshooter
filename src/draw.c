#include "draw.h"
#include "structs.h"
#include <SDL2/SDL_image.h>

extern App app;

void prepareScene(void) {
    SDL_SetRenderDrawColor(app.renderer, 32, 32, 32, 255);
    SDL_RenderClear(app.renderer);
}

void presentScene(void) { SDL_RenderPresent(app.renderer); }

SDL_Texture* loadTexture(char* filename) {
    SDL_Texture* texture;
    SDL_LogMessage(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO, "Loading %s", filename);
    texture = IMG_LoadTexture(app.renderer, filename);

    return texture;
}

void blit(SDL_Texture* texture, SDL_Rect src, SDL_Rect dst) {
    // SDL_QueryTexture(texture, NULL, NULL, &src.w, &src.h);
    SDL_RenderCopyEx(app.renderer, texture, &src, &dst, 90, NULL, SDL_FLIP_NONE);
}
